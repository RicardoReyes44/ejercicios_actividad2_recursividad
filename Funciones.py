'''
Created on 7 oct. 2020

@author: RSSpe
'''

def factorial(numero):
    if numero==1:
        return 1
    else:
        return numero * factorial(numero-1)


def divisores(numero, numero2):
    if numero2>0:
        if numero%numero2==0:
            print(numero2)
        return divisores(numero, numero2-1)


def cocientesEnteros(menor, mayor, cam):
    if cam>=menor:
        cociente = mayor/cam;
        if mayor%cam==0:
            print(f"El cociente de {mayor}/{cam} = {cociente}");
        cocientesEnteros(menor, mayor, cam-1);


def divisoresEnteros(menor, mayor, cam):
        if cam>=menor:
            mas=0
            if mayor%cam==0:
                mas+=1
            return mas + divisoresEnteros(menor, mayor, cam-1);
        else:
            return 0


def cantidadVocales(cadena, longitud):
    if longitud==-1:
        return 0
    else:
        vocal = 0
        if ord(cadena[longitud])==97 or ord(cadena[longitud])==101 or ord(cadena[longitud])==105 or ord(cadena[longitud])==111 or ord(cadena[longitud])==117:
            vocal+=1;
        return vocal + cantidadVocales(cadena, longitud-1)


def cadenaInversa(cadena, longitud):
    if longitud==0:
        print(cadena[longitud], end="")
    else:
        print(cadena[longitud], end="")
        cadenaInversa(cadena, longitud-1)


def conversion(numero):
    if int(numero/2)==0:
        return "1"
    else:
        n = "1"
        if numero%2==0:
            n = "0"
        return n + conversion(int(numero/2))
            