'''
Created on 9 oct. 2020

@author: RSSpe
'''

from Funciones import cadenaInversa


cadena = input("Introduce cadena: ").lower()

print("Cadena inversa: ", end="")
cadenaInversa(cadena, len(cadena)-1)
